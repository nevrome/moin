---
title: Modeling Interactions between 17th century settlements
author: Moin summer school participants
---

```{r setup}
library(magrittr)
```

We employ a geopackage dataset with point locations of settlements from the 17th century in Eastern Schleswig-Holstein, Germany. The data was digitized from "Landtcarte Von dem Lande Wageren. Welches ist das Ostertheil von Holstein" created 1651 by Johannes Meyer (Iohannes Mejerus).

![Map of 17th century Wagrien, Eastern Schleswig-Holstein, Germany](../vignettes/data/OldenburgerGraben/00009457.jpg)

```{r LoadData}
library(moin)
medieval_settlements <- sf::st_read(
  dsn = "../vignettes/data/OldenburgerGraben/Map_data_Meyer.gpkg",
  layer = "Settlements") %>%
  dplyr::slice(1:10)

mapview::mapview(medieval_settlements)
```

Connect all points within a distance of 4 km.

```{r, CreateNetwork}
mdm <- moin::moin_network(
  input = medieval_settlements,
  method = "mdm",
  par = 4000)

plot(mdm$graph,
     vertex.size=3,
     vertex.label.cex=0.25,
     layout=sf::st_coordinates(mdm$input_data))
```

Calculate Interactions

```{r calculateInteractions}
costs <- inverse_exponential(mdm$distance_matrix)
test_d <- matrix(runif(length(costs)),ncol(costs),nrow(costs))

rw_model <- moin::metropolis(
  hfunc = h_simple_gravity,
  hvars = list(E = test_d),
  hconsts = list(beta = 64,
                 alpha = 64,
                 Si = 1,
                 Sj = mdm$input_data$rank,
                 C = costs, 
                 c = 1,
                 f = 1),
  hvar_constraints = list(E = c(0,1)),
  beta = 100,
  threshold = 1)

rw_model
```
